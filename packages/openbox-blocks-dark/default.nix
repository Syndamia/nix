{ lib, stdenvNoCC, fetchFromGitLab, }:
stdenvNoCC.mkDerivation rec {
  pname = "openbox-blocks-dark";
  version = "1.0.0";

  src = fetchFromGitLab {
    owner = "Syndamia";
    repo = "openbox-blocks-dark";
    rev = "d9e87f8c4494f1768d61c3f46ecfb136b6f168e4";
    sha256 = "sha256-ASeTBTCcTrVgqy/6MYfyiBMh4/CW5z4epPpQPuzNwMw=";
  };

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/themes
    mv Blocks-Dark $out/share/themes
  '';

  meta = with lib; {
    description = "A dark version of the \"Blocks\" theme found on github.com/addy-dclxvi/openbox-theme-collections";
    homepage = "https://gitlab.com/Syndamia/openbox-blocks-dark";
    license = licenses.gpl3;
    platforms = platforms.linux;
    maintainers = with maintainers; [ syndamia ];
  };
}
