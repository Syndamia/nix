{ lib, stdenvNoCC, fetchFromGitLab, }:
stdenvNoCC.mkDerivation rec {
  pname = "oreo-black-bordered-cursor";
  version = "1.0.0";

  src = fetchFromGitLab {
    owner = "Syndamia";
    repo = "oreo-black-bordered-cursors";
    rev = "bd89343f5d090a03f211610a013e401e25e11c09";
    sha256 = "sha256-mq3mtwSPDBJ1y6iwFZsCfD7ca+YUl7lHKYtL3u0KlcI=";
  };

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/icons
    mv oreo_black_bordered_cursors $out/share/icons
  '';

  meta = with lib; {
    description = "Custom set of oreo cursors";
    homepage = "https://gitlab.com/Syndamia/oreo-black-bordered-cursors";
    license = licenses.gpl2;
    platforms = platforms.linux;
    maintainers = with maintainers; [ syndamia ];
  };
}
