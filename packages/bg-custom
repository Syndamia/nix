//////////////////////////////////////////////////////////////////////////
// Copyright (C) 1999, 2000, 2007, 2009 by Anton Zinoviev <anton@lml.bas.bg>
//
// This software may be used, modified, copied, distributed, and sold,
// both in source and binary form provided that the above copyright
// notice and these terms are retained. The name of the author may not
// be used to endorse or promote products derived from this software
// without prior permission.  THIS SOFTWARE IS PROVIDED "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES ARE DISCLAIMED.  IN NO EVENT
// SHALL THE AUTHOR BE LIABLE FOR ANY DAMAGES ARISING IN ANY WAY OUT
// OF THE USE OF THIS SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////

// Version 1.1 // Fork by Kamen Mladenov <kamen@syndamia.com>

// This is a non-standard variant of the traditional Bulgarian phonetic
// keyboard, which allows for usage of some English characters that aren't
// normally supported.

// Original English characters are returned in the following places:
// - <Shift> + <Any Digit Key from 1 to 0>
// - <3rd level key> + <[>   and   <3rd level key> + <Shift> + <[>
// - <3rd level key> + <]>   and   <3rd level key> + <Shift> + <]>
// - <3rd level key> + <`>   and   <3rd level key> + <Shift> + <`>

// Version History:
// [03.12.2022] 1.1 - Updated description and added Backtick and Tilda to
//                    third (and fourth) level layouts.
// [03.12.2022] 1.0 - Initial

// Special thanks to Daniel Paul O'Donnell for his blog post:
// https://people.uleth.ca/~daniel.odonnell/Blog/custom-keyboard-in-linuxx11

//////////////////////////////////////////////////////////////////////////

default partial alphanumeric_keys
xkb_symbols "phonetic-nonstandard" {
  name[Group1]= "Bulgarian (traditional phonetic, nonstandard)";

  key <TLDE> {[ Cyrillic_che,      Cyrillic_CHE,      grave,             asciitilde          ]};
  key <AE01> {[ 1,                 exclam                                                    ]};
  key <AE02> {[ 2,                 at                                                        ]};
  key <AE03> {[ 3,                 numbersign                                                ]};
  key <AE04> {[ 4,                 dollar,            EuroSign,          EuroSign            ]};
  key <AE05> {[ 5,                 percent                                                   ]};
  key <AE06> {[ 6,                 asciicircum                                               ]};
  key <AE07> {[ 7,                 ampersand                                                 ]};
  key <AE08> {[ 8,                 asterisk                                                  ]};
  key <AE09> {[ 9,                 parenleft,         bracketleft,       U2329               ]};
  key <AE10> {[ 0,                 parenright,        bracketright,      U232A               ]};
  key <AE11> {[ minus,             underscore,        U2011,             U2011               ]};
  key <AE12> {[ equal,             plus,              emdash,            dagger              ]};

  key <AD01> {[ Cyrillic_ya,       Cyrillic_YA,       U0463,             U0462               ]};
  key <AD02> {[ Cyrillic_ve,       Cyrillic_VE                                               ]};
  key <AD03> {[ Cyrillic_ie,       Cyrillic_IE,       Cyrillic_e,        Cyrillic_E          ]};
  key <AD04> {[ Cyrillic_er,       Cyrillic_ER,       registered,        registered          ]};
  key <AD05> {[ Cyrillic_te,       Cyrillic_TE,       trademark,         trademark           ]};
  key <AD06> {[ Cyrillic_hardsign, Cyrillic_HARDSIGN, U046B,             U046A               ]};
  key <AD07> {[ Cyrillic_u,        Cyrillic_U                                                ]};
  key <AD08> {[ Cyrillic_i,        Cyrillic_I,        U045D,             U040D               ]};
  key <AD09> {[ Cyrillic_o,        Cyrillic_O                                                ]};
  key <AD10> {[ Cyrillic_pe,       Cyrillic_PE                                               ]};
  key <AD11> {[ Cyrillic_sha,      Cyrillic_SHA,      bracketleft,       braceleft           ]};
  key <AD12> {[ Cyrillic_shcha,    Cyrillic_SHCHA,    bracketright,      braceright          ]};

  key <AC01> {[ Cyrillic_a,        Cyrillic_A                                                ]};
  key <AC02> {[ Cyrillic_es,       Cyrillic_ES,       copyright,         copyright           ]};
  key <AC03> {[ Cyrillic_de,       Cyrillic_DE                                               ]};
  key <AC04> {[ Cyrillic_ef,       Cyrillic_EF                                               ]};
  key <AC05> {[ Cyrillic_ghe,      Cyrillic_GHE                                              ]};
  key <AC06> {[ Cyrillic_ha,       Cyrillic_HA                                               ]};
  key <AC07> {[ Cyrillic_shorti,   Cyrillic_SHORTI,   U046D,             U046C               ]};
  key <AC08> {[ Cyrillic_ka,       Cyrillic_KA                                               ]};
  key <AC09> {[ Cyrillic_el,       Cyrillic_EL                                               ]};
  key <AC10> {[ semicolon,         colon,             ellipsis,          ellipsis            ]};
  key <AC11> {[ apostrophe,        quotedbl,       rightsinglequotemark, leftsinglequotemark ]};
  key <BKSL> {[ Cyrillic_yu,       Cyrillic_YU                                               ]};

  key <LSGT> {[ U045D,             U040D                                                     ]};
  key <AB01> {[ Cyrillic_ze,       Cyrillic_ZE                                               ]};
  key <AB02> {[ Cyrillic_softsign, U045D,             Cyrillic_yeru,     Cyrillic_YERU       ]};
  key <AB03> {[ Cyrillic_tse,      Cyrillic_TSE,      copyright,         copyright           ]};
  key <AB04> {[ Cyrillic_zhe,      Cyrillic_ZHE                                              ]};
  key <AB05> {[ Cyrillic_be,       Cyrillic_BE                                               ]};
  key <AB06> {[ Cyrillic_en,       Cyrillic_EN                                               ]};
  key <AB07> {[ Cyrillic_em,       Cyrillic_EM                                               ]};
  key <AB08> {[ comma,            doublelowquotemark, guillemotleft,     guillemotleft       ]};
  key <AB09> {[ period,          leftdoublequotemark, guillemotright,    guillemotright      ]};
  key <AB10> {[ slash,             question,          U0300,             U0301               ]};

  key <SPCE> {[ space,             space,             nobreakspace,      nobreakspace        ]};

  key <KPDL> { type[Group1] = "KEYPAD",             [ KP_Delete,         KP_Separator        ]};
};

