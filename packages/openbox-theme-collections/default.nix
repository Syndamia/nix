{ lib, stdenvNoCC, fetchFromGitHub, }:
stdenvNoCC.mkDerivation rec {
  pname = "openbox-theme-collections";
  version = "1.0.0";

  src = fetchFromGitHub {
    owner = "addy-dclxvi";
    repo = "openbox-theme-collections";
    rev = "8bdf5decb25c9867d11011f386453380fc908121";
    sha256 = "sha256-aC6AA09S/NE74fFNQXK8R/AVA3w4JWKjhcgEkCtGGdk=";
  };

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/themes
    find . ! -name . -prune -type d -exec mv {} $out/share/themes \;
  '';

  meta = with lib; {
    description = "Addy's Openbox theme collections";
    homepage = "https://github.com/addy-dclxvi/openbox-theme-collections";
    license = licenses.gpl3;
    platforms = platforms.linux;
    maintainers = with maintainers; [ syndamia ];
  };
}
