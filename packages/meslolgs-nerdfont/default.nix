{ lib, stdenvNoCC, fetchFromGitHub, }:
stdenvNoCC.mkDerivation rec {
  pname = "meslolgs-nerdfont";
  version = "2.3.3";

  src = fetchFromGitHub {
    owner = "romkatv";
    repo = "powerlevel10k-media";
    rev = "145eb9fbc2f42ee408dacd9b22d8e6e0e553f83d";
    sha256 = "sha256-8xwVOlOP1SresbReNh1ce2Eu12KdIwdJSg6LKM+k2ng=";
  };

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/fonts/truetype/MesloLGS-NF
    find . -name \*.ttf -exec mv {} $out/share/fonts/truetype/MesloLGS-NF \;
  '';

  meta = with lib; {
    description = "Powerlevel10k's font";
    homepage = "https://github.com/romkatv/powerlevel10k/blob/master/font.md";
    license = licenses.mit;
    platforms = platforms.linux;
    maintainers = with maintainers; [ syndamia ];
  };
}
