final: prev: {
  xed-editor = prev.xed-editor.overrideAttrs (prevAttrs: {
    buildInputs = prevAttrs.buildInputs ++ (with final; [
      gobject-introspection
      python3.pkgs.pygobject3
    ]);

    patches = [ ./libxed.patch ];

    preFixup = with final; ''
      gappsWrapperArgs+=(
        --prefix PYTHONPATH : "${python3.pkgs.pygobject3}/${python3.sitePackages}:$out/lib/xed/plugins/"
      )
    '';
  });
}
