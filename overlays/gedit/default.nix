final: prev: {
  gedit = prev.gedit.overrideAttrs (prevAttrs: {
    buildInputs = prevAttrs.buildInputs ++ (with final; [
      gobject-introspection
      python3.pkgs.pygobject3
    ]);

    preFixup = with final; ''
      gappsWrapperArgs+=(
        --prefix PYTHONPATH : "${python3.pkgs.pygobject3}/${python3.sitePackages}:$out/lib/libgedit/plugins/"
      )
    '';
  });
}
