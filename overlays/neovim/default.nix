final: prev: {
  neovim-unwrapped = prev.neovim-unwrapped.overrideAttrs (prevAttrs: {
    patches = (prevAttrs.patches or []) ++
              (if prevAttrs.version == "0.9.5" then [ ./26892.patch ] else []); # Terminal insert-mode mouse check patch
  });
}
