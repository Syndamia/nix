let
  # https://github.com/NixOS/nixpkgs/blob/607aeee18d5c4de0cddfdfbebd595930051c9618/lib/trivial.nix#L504-L515
  inPureEvalMode = ! builtins ? currentSystem;

  rev = "ad7af231a95acf65ccc4afa0c766f5c0674ad3f1";
  src = builtins.fetchTarball {
      url = "https://github.com/mozilla/nixpkgs-mozilla/archive/${rev}.tar.gz";
      # This locks the overlay definition version, but **not** the packages version
      sha256 = "sha256:0j7m3ay5zcnijg0vqnqyf618n66x5zgk9p4j2zas1c0gyqzh1bqr";
    };
in
  final: prev: {
    mozilla = if inPureEvalMode then
                  abort "mozilla/nixpkgs-mozilla firefox packages are not reproducible! Use --impure!"
              else
                  ((import "${src}/firefox-overlay.nix") final prev).latest;
  }
