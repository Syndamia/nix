rec {
  barebones = [ "cli-essential" "fonts" "users-standard" ];

  personal = barebones ++ [ # default.nix is always included by autoconfig
    "hardware-general" "virtualization"
    # GUI
    "gui-essential" "xserver" "openbox-de"
    # Work/development
    "cli-powerful" "dev-cli" "tex" "web-server"
    # Custom hardware
    "keyboards-custom" "rtl-sdr"
  ];

  desktop = personal ++ [
    "dev-gui" "gui-powerful"
  ];
  laptop = personal ++ [
    "battery" "hardware-laptop"
  ];
}
