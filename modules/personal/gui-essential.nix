{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    devilspie2 sxhkd speedcrunch peek wireshark
    obs-studio qbittorrent glava
    keepassxc
    # Terminals
    terminator xterm
    # Settings and backup tools
    feh arandr pavucontrol blueberry piper
    # Tray
    copyq flameshot barrier redshift
    # File openers/players
    audacious mpv libreoffice
    # File editors
    gimp xed-editor
    # Web (app) clients
    brave nixpkgs-2305.bitwarden nextcloud-client thunderbird birdtray
  ];

  nixpkgs.config.permittedInsecurePackages = [
    "qbittorrent-4.6.4"
  ];

  programs.firefox.enable = true;

  systemd.user.services = {
    devilspie2 = {
      description = "devilspie2 runner";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        ExecStart = "${pkgs.devilspie2}/bin/devilspie2";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };
  };
}
