{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    minikube
  ];

  services.k3s = {
    enable = true;
    extraFlags = "--kubelet-arg='eviction-hard=nodefs.available<1%,imagefs.available<1%,nodefs.inodesFree<1%' --write-kubeconfig-mode=644";
  };
}
