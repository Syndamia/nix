{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    neovim cron
    nixpkgs-unstable.yash
    # Hardware
    usbutils chrony iotop efibootmgr libva-utils
    # File systems
    parted dosfstools mtpfs ntfs3g squashfsTools
    # Network
    vnstat yt-dlp dnsutils
    # Working with files
    neovim inotify-tools lsof tree xmlstarlet
    # Working with (media) files
    ffmpeg img2pdf pdftk imagemagick pngcrush jpegoptim optipng gd kid3 pandoc libisoburn mediainfo exiftool
    cdrtools rpm dpkg
    # Misc
    neofetch nixpkgs-2305.exa moc lazygit aspell ddrescue
  ];
}
