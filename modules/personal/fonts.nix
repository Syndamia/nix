{ pkgs, ... }: {
  fonts.packages = with pkgs; [
    meslolgs-nerdfont
    # Note to self: DO NOT INSTALL ADOBE FONTS!
    corefonts vistafonts liberation_ttf helvetica-neue-lt-std noto-fonts noto-fonts-emoji cm_unicode #symbola
    # CJK
    noto-fonts-cjk-sans arphic-ukai arphic-uming takao wqy_microhei wqy_zenhei
  ];
}
