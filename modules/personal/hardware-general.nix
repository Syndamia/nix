{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    pipewire bluez
  ];

  programs = {
    mtr.enable = true;
  };

  #
  # Graphics
  #

  # https://nixos.wiki/wiki/Accelerated_Video_Playback
  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  #
  # Sound
  #

  # hardware.pulseaudio.enable = true;

  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };
  services.dbus = {
    enable = true;
    packages = [ pkgs.dconf ];
  };

  #
  # Bluetooth
  #

  hardware.bluetooth.enable = true;

  #
  # Printers and scanners
  #

  # Enable CUPS to print documents.
  services.printing.enable = true;
  hardware.sane.enable = true;

  #
  # Other hardware
  #

  hardware.rtl-sdr.enable = true;
}
