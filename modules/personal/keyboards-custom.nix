{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    via qmk
  ];
  services.udev.packages = with pkgs; [
    via qmk
  ];
  hardware.keyboard.qmk.enable = true;
}
