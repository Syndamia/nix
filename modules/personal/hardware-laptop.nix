{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    brightnessctl
    libinput libinput-gestures
    epkowa
    (epsonscan2.override { withNonFreePlugins = true; withGui = true; })
  ];

  hardware.sane.extraBackends = [ (pkgs.epsonscan2.override { withNonFreePlugins = true; withGui = true; }) ];

  # touchpad support
  services.libinput = {
    enable = true;
    touchpad.naturalScrolling = true;
  };
}
