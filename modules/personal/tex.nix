{ pkgs, ... }:
let
  tex = (pkgs.texlive.combine {
    inherit (pkgs.texlive) scheme-medium
    babel-bulgarian cyrillic cancel enumitem;
  });
in {
  environment.systemPackages = with pkgs; [
    texmaker tex
  ];
}

