{ config, pkgs, ... }: {
  # zsh
  programs.zsh.enable = true;
  environment.shells = with pkgs; [ zsh ];
  # Thanks https://discourse.nixos.org/t/gns3-setcap-and-ubridge-gns3-unable-to-execute-setcap-command/13485/3
  users = {
    groups.ubridge = {};
    users.kamen = {
      isNormalUser = true;
      extraGroups = [ "wheel" "audio" "video" "input" "lpadmin" "libvirtd" "lp" "scanner" "plugdev" "docker" "ubridge" ]; # Enable ‘sudo’ for the user.
      packages = [ ];
      shell = pkgs.bash;
      homeMode = "711"; # mostly done for httpd access
    };
  };
}
