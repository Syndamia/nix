{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    # File/image/audio editors
    inkscape krita ardour
    # System
    gparted timeshift
  ];
}
