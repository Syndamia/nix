{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    dash htop rlwrap bc
    # Hardware
    lm_sensors lshw
    # Network
    wget git rsync inetutils
    # Files
    vim-full nnn
    # Archive formats
    gnutar p7zip zip unzip rar unrar
  ];

  programs = {
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryPackage = pkgs.pinentry-all;
    };
  };
}
