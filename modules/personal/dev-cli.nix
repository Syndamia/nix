{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    gnumake just
    lowdown wakatime cloc go php fontforge ruby csslint
    # C/C++
    clang clang-tools glibc gcc gdb valgrind pugixml cmake libusb1 libusb-compat-0_1 meson
    # .NET
    dotnet-sdk mono
    # Python
    python3Minimal
    # Android
    android-tools heimdall
    # awk
    gawk mawk nawk
    # DB
    sqlite sqlitebrowser flyway
    # Functional
    racket ghc sbcl
    # Compilers
    flex bison
  ];

  services.mysql.enable = false;
  services.mysql.package = pkgs.mysql;
}
