{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    virt-manager libvirt qemu_kvm kvmtool qemu_full
  ];

  virtualisation = {
    libvirtd.enable = true;
    docker.enable = true;
  };
}
