{ pkgs, ... }: {
  services.xserver = {
    # xkbmap
    xkb.layout = "us,bg-custom";
    xkb.variant = ",phonetic-nonstandard";
    xkb.options = "grp:rwin_toggle,grp:alt_shift_toggle,lv3:switch,caps:escape";
    xkb.extraLayouts.bg-custom = {
      description = "Non-standard variant of the traditional Bulgarian phonetic keyboard, which replaces some characters with their English counterparts";
      languages   = [ "bg" ];
      symbolsFile = ../../packages/bg-custom;
    };
  };
}
