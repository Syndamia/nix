{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    apacheHttpd
  ];

  services.httpd = {
    enable = true;

    virtualHosts = {
      syndamiadotcom-local = {
        hostName = "synd.local";
        listen = [ { port = 80; } ];
        documentRoot = "/home/kamen/Programming/GitLab-repos/syndamiadotcom/website";
      };
    };
  };

  networking.extraHosts = ''
    127.0.0.1 synd.local
  '';
}
