{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    # Networking
    gns3-gui gns3-server dynamips
    # Other
    rstudio
  ];
}
