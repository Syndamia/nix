{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    # Essential libs
    dbus cairo libnotify gtk3 yad zenity gnome.gvfs at-spi2-atk
    # Main apps and their config apps
    # window manager, compositor, taskbar, notifications, app menu, lockscreen, session manager
    openbox lxappearance obconf picom nixpkgs-2305.tint2 dunst jgmenu i3lock-color lightdm lightdm-gtk-greeter
    # General X11 tools
    lxqt.lxqt-policykit xorg.xev xfontsel xorg.xmodmap xorg.xwininfo xorg.xprop clipnotify xdotool xautolock xcape xorg.setxkbmap xkbmon xorg.xhost xclip wmctrl
    menu-cache
    # Tray
    libappindicator networkmanagerapplet pnmixer
    # File openers
    evince eog nixpkgs-2305.gnome.file-roller nemo-with-extensions seahorse
    # System
    gnome-system-monitor udisks gnome-disk-utility baobab simple-scan
    # Theme
    nixpkgs-mint.cinnamon.mint-themes nixpkgs-mint.cinnamon.mint-y-icons oreo-black-bordered-cursor
    openbox-theme-collections openbox-blocks-dark

    xdg-desktop-portal-gtk xdg-desktop-portal-xapp
  ];

  security.polkit.enable = true;

  xdg.portal = {
    enable = true;
    configPackages = with pkgs; [ xdg-desktop-portal-gtk xdg-desktop-portal-xapp ];
    config.common = {
      default = [ "gtk" "xapp" ];
      "org.freedesktop.impl.portal.Screenshot" = [ "xapp" ];
    };
  };

  programs = {
    dconf.enable = true;
    gnome-disks.enable = true;
  };

  services = {
    dbus.packages = with pkgs; [ gnome2.GConf ];
    udev.packages = with pkgs; [ gnome-settings-daemon ];

    udisks2.enable = true; # gnome-disks disk access
    gnome.gnome-keyring.enable = true;
    gvfs.enable = true; # MTP (for nemo)

    logind.extraConfig = ''
      InhibitDelayMaxSec=1
      HoldoffTimeoutSec=1s
      HandlePowerKey=poweroff'';
  };

  services.xserver = {
    enable = true;
    # Window manager
    windowManager.openbox.enable = true;

    xautolock = {
      enable = true;
      time = 15;
      locker = "/home/kamen/.a/sys/lock.sh";
      extraOptions = [ "-detectsleep" "-secure" ];
    };
  };

  systemd.services = {
    lockscreen = {
      wantedBy = [ "sleep.target" ];
      before = [ "suspend.target" ];
      serviceConfig = {
        User = "kamen";
        Environment = [ "DISPLAY=:0" "PATH=/run/current-system/sw/bin" "DBUS_SESSION_BUS_ADDRESS=\"unix:path=/run/user/1000/bus\"" ];
        ExecStart = "/home/kamen/.a/sys/lock.sh";
      };
    };
  };

  systemd.user.services = {
    lxqt-policykit = {
      description = "lxqt-policykit runner";
      wantedBy = [ "graphical-session.target" ];
      wants = [ "graphical-session.target" ];
      after = [ "graphical-session.target" ];
      serviceConfig = {
        Type = "simple";
        ExecStart = "${pkgs.lxqt.lxqt-policykit}/bin/lxqt-policykit-agent";
        Restart = "on-failure";
        RestartSec = 1;
        TimeoutStopSec = 10;
      };
    };

    # This is kinda gross, because the xautolock service is already defined and configured
    # inside services.xserver.xautolock, but it's the only way I know
    xautolock.serviceConfig.Environment = [ "DISPLAY=:0" "PATH=/run/current-system/sw/bin" ];
  };
}
