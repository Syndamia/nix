{ config, pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    networkmanager
  ];

  #
  # Kernel
  #

  boot.kernelModules = [ "kvm-intel" ];

  #
  # Bootloader
  #

  # Use the systemd-boot EFI boot loader.
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # Use the GRUB 2 boot loader
  boot.loader = {
    grub = {
      enable = true;
      copyKernels = true;
      efiSupport = true;
      efiInstallAsRemovable = true;
      # Define on which hard drive you want to install Grub
      device = "nodev"; # or "nodev" for efi only
    };
    timeout = 1;
  };

  #
  # Timezone and locale
  #

  # Set your time zone.
  time.timeZone = "Europe/Sofia";
  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  #
  # Networking
  #

  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # zeroconf networking
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };
}
