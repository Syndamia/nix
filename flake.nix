{
  description = "System setup";

  inputs = {
    nixpkgs.url      = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-2305.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";

    nixpkgs-mint.url = "github:nixos/nixpkgs/9217c8084d8bc067d48c8edf5768e9dc54962970"; # 10 Aug 2022
  };

  outputs = inputs:
  let
    autoconfig = import ./autoconfig.nix { inherit inputs; };
  in {
    nixosConfigurations = autoconfig.configurations;
  };
}
