{ inputs, ... }:
let
  inherit (inputs) nixpkgs;
  inherit (nixpkgs) lib;
in {
  configurations =
    lib.mapAttrs
    (dir: _:
      let
        # sysidentity.nix is a file inside machines/name/, which is always in the form
        # groups: # attrset which will be used in include and exclude
        # {
        #   name = ""; # hostname of machine
        #   arch = ""; # architecture
        #   kind = ""; # name of subfolder inside ./modules
        #   include = []; # names of files inside ./modules/kind/ which to be included, by default only default.nix is
        #   exclude = []; # names of files inside ./modules/kind/ which to be excluded (from include)
        # }
        sysidentity = (import ./machines/${dir}/sysidentity.nix) (import ./module-groups.nix);

        modules-dir = lib.path.append ./modules sysidentity."kind";

        package-modules =
          let
            getmodules = attrname:
              if sysidentity ? ${attrname}
              then map
                    (v: lib.path.append modules-dir (v + (if lib.hasSuffix ".nix" v then "" else ".nix")))
                    sysidentity.${attrname}
              else [];

            includelist = getmodules "include";
            excludelist = getmodules "exclude";
          in
            lib.subtractLists excludelist includelist;

        inputs-overlays =
          lib.mapAttrs
          (_: nixpkgs-special:
            nixpkgs-special.legacyPackages.${sysidentity.arch})
          (lib.filterAttrs
            (name: _:
              lib.hasPrefix "nixpkgs-" name)
            inputs);

        custom-packages-overlays =
          lib.mapAttrs
          (name: _:
            nixpkgs.legacyPackages.${sysidentity.arch}.callPackage ./packages/${name} { })
          (lib.filterAttrs (_: type: type == "directory") (builtins.readDir ./packages));

        package-overlays =
          lib.mapAttrsToList
          (name: _: (import ./overlays/${name}))
          (builtins.readDir ./overlays);

      in
      lib.nixosSystem {
        system = sysidentity.arch;
        modules = [
          ./machines/${dir}/hardware-configuration.nix
          ./configuration.nix
          modules-dir
          ({ ... }: {
            networking.hostName = sysidentity.name;
            nixpkgs.overlays = [ (_: _: inputs-overlays) (_: _: custom-packages-overlays) ] ++ package-overlays;
          })
        ]
        ++ (if lib.pathIsRegularFile ./machines/${dir}/configuration.nix
            then [ ./machines/${dir}/configuration.nix ]
            else [])
        ++ package-modules;
      })
    (lib.filterAttrs (_: type: type == "directory") (builtins.readDir ./machines));
}
