groups: {
  name = "minerva";
  arch = "x86_64-linux";
  kind = "personal";

  include = groups.laptop ++ [ "gui-powerful" ];
}
