groups: {
  name = "ceres";
  arch = "x86_64-linux";
  kind = "personal";

  include = groups.desktop ++ [ "work" ];
}
