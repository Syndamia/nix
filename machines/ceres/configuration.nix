{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    alsa-plugins radeontop glxinfo vulkan-tools clinfo gpu-viewer lutris wine gns3-server gns3-gui docker tigervnc ubridge vpcs
    unigine-heaven unigine-superposition
    prismlauncher
  ];

  networking.firewall.enable = false;

  security.wrappers.ubridge = {
    source = "${pkgs.ubridge}/bin/ubridge";
    capabilities = "cap_net_admin,cap_net_raw=ep";
    owner = "root";
    group = "ubridge";
    permissions = "u+rx,g+rx,o+rx";
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };

  # https://github.com/NixOS/nixpkgs/issues/6860
  environment.sessionVariables = {
    ALSA_PLUGIN_DIR = pkgs.alsa-plugins.outPath + "/lib/alsa-lib";
  };
}
