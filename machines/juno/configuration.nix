{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    thinkfan xorg.xf86inputsynaptics
  ];

  # External third-party battery is no longer able to hold 100% charge without getting "stuck"
  # Often this would result in an immediate shutdown, since there would be either a short or voltage surge
  # My guess is the problem lies with the controller
  # I've twice been able to rejuvenate it by just retrying and retrying to discharge it
  # So, this service is here, if that happens again, it should be enabled and then on every bootup when charger is connected
  # it will try to discharge battery, which will result in shutdown.
  systemd.services.extdrain = {
    enable = false;
    wantedBy = [ "multi-user.target" ];
    unitConfig = {
      Type = "simple";
    };
    path = [ pkgs.tlp ];
    serviceConfig = {
      ExecStart = "${pkgs.tlp}/bin/tlp discharge BAT1";
    };
  };

  services.thinkfan = {
    enable = true;
    levels = [
      [0  0 35]
      [1 35 38]
      [3 38 53]
      ["level auto" 53 32767]
    ];
  };
}
