groups: {
  name = "vesta";
  arch = "x86_64-linux";
  kind = "personal";

  include = groups.desktop;
}
