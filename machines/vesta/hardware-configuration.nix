# Machine-specific kernel, cpu, gpu and file system settings
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "firewire_ohci" "usbhid" "usb_storage" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" "amdgpu" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/766f2ace-7300-45c0-aaba-25b84da5c308";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/269D-7930";
      fsType = "vfat";
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/0197b42b-12e0-4d7e-b65b-cdd9bed07eda";
      fsType = "ext4";
    };

  # fileSystems."/mnt/Svalbard" =
  #   { device = "/dev/disk/by-uuid/5bd27e26-5b83-4485-ab3f-bcfbbd2b40d5";
  #     fsType = "ext4";
  #     options = [ "defaults" "x-gvfs-show" ];
  #   };

  # fileSystems."/mnt/Norilsk" =
  #   { device = "/dev/disk/by-uuid/689f863a-5026-42ff-aecc-3195e8d8c684";
  #     fsType = "ext4";
  #     options = [ "defaults" "x-gvfs-show" ];
  #   };

  swapDevices = [ { device = "/home/swapfile"; size = 24 * 1024; } ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s25.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;

  # GPU
  # There is a potential bug with amdgpu driver (https://bugzilla.mozilla.org/show_bug.cgi?id=1437520#c4)
  # Remember to disable VAAPI for this machine in Firefox: media.ffmpeg.vaapi.enabled = false (https://wiki.archlinux.org/title/Firefox#Hardware_video_acceleration)

  #boot.initrd.kernelModules = [ "amdgpu" ];
  services.xserver.videoDrivers = [ "amdgpu" ];

  boot.kernelParams = [
    "radeon.si_support=0" "amdgpu.si_support=1" # for Southern Islands (SI ie. GCN 1) cards
    "libata.force=2.00:nodmalog" # Eliminate "qc timeout" 15 second boot delay for /home SSD
  ];
  # systemd.tmpfiles.rules = [
  #   "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}"
  # ];
  hardware.opengl = {
    # Mesa
    enable = true;

    extraPackages = with pkgs; [
      # OpenCL
      # rocmPackages.clr
      # VA-API
      libva
      # AMDVLK
      # amdvlk
    ];
    # extraPackages32 = with pkgs; [
    #   driversi686Linux.amdvlk
    # ];

    # Vulkan
    driSupport = true;
    # For 32 bit applications
    driSupport32Bit = true;
  };
  # system.replaceRuntimeDependencies = [
  #   ({ original = pkgs.mesa; replacement = (import /srv/nixpkgs-mesa { }).pkgs.mesa; })
  #   ({ original = pkgs.mesa.drivers; replacement = (import /srv/nixpkgs-mesa { }).pkgs.mesa.drivers; })
  # ];
}
