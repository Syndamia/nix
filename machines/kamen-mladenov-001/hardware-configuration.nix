{
  config,
  lib,
  pkgs,
  modulesPath,
  dirs,
  ...
}: {
  imports = [(modulesPath + "/installer/scan/not-detected.nix")];
  disko = (import "${dirs.lib}/disko.nix").makeZfsPartitions {
    disks = ["/dev/disk/by-id/nvme-KINGSTON_SKC3000D2048G_50026B7686C1B5FA" "/dev/disk/by-id/nvme-KINGSTON_SKC3000D2048G_50026B7686C1B66E"];
    swapSizeGB = 96;
    espSizeGB = 4;
    inherit config;
  };
  boot = {
    initrd = {
      kernelModules = [];
      availableKernelModules = [
        "ahci"
        "nvme"
        "rtsx_usb_sdmmc"
        "sd_mod"
        "usb_storage"
        "usbhid"
        "vmd"
        "xhci_pci"
      ];
    };
    kernelModules = ["kvm-amd"];
    extraModulePackages = [];
    loader = {
      systemd-boot.enable = true;
      grub = {
        enable = false;
        efiSupport = true;
        copyKernels = true;
      };
      efi.canTouchEfiVariables = lib.mkDefault true;
    };
    blacklistedKernelModules = [];
  };
  networking.useDHCP = lib.mkDefault true;
  powerManagement.cpuFreqGovernor = lib.mkDefault "performance";
  hardware = {
    cpu.amd.updateMicrocode = true;
    enableAllFirmware = true;
    enableRedistributableFirmware = lib.mkDefault true;
  };
}
